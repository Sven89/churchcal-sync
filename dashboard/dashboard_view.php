<div>
 	<h2>Settings for ChurchTools Importer</h2>
 	<div>Just modify the fields below:</div>
 	<div>
 		<form method="post" class="ctsync_settings" action="" data-action="save_ctsync_settings">
		<br>ChurchTools-URL<br>
		<input type="text" size="30" name="ctsync_url" id="ctsync_url" class="text_box" placeholder="https://yourchurch.church.tools/" value="<?php echo $saved_data ? $saved_data['url'] : '' ; ?>" required> 
		<br>ChurchTools User ID and authentification token (Only required if non-public calendars should be shown)<br>
		<input type="text" size="30" name="ctsync_userid" id="ctsync_userid" class="text_box" placeholder="<id-of-user>" value="<?php echo $saved_data ? $saved_data['userid'] : '' ; ?>"> 
		<input type="password" size="30" name="ctsync_logintoken" id="ctsync_logintoken" class="text_box" placeholder="my login token" value="<?php echo $saved_data ? $saved_data['logintoken'] : '' ; ?>"> 
		<br>Category IDs (for example 42,43,52)<br>
		<input type="text" size="30" name="ctsync_ids" id="ctsync_ids" class="text_box" placeholder="42,43,52" value="<?php echo $saved_data ? implode(', ',$saved_data['ids']) : '' ; ?>" required> 
		<br>Max-Calendar Days (Import)<br>
		<input type="text" size="30" name="ctsync_maximport" id="ctsync_maximport" class="text_box" placeholder="100" value="<?php echo $saved_data ? $saved_data['maximport'] : '' ; ?>" required> 
		<input type="submit" value="Save">
		<p><strong>Last updated:</strong> <?php echo $lastupdated; ?></p>
 	</div>
</div>
