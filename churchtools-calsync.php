<?php
/*
Plugin Name: ChurchTools Calendar-Sync
Description: This plugin is synchronizing the ChurchTools ChurchCal with Wordpress by adding a custom cronjob event to check updates every hour. Based on the Plugin of Thomas Stadelmann (https://forum.churchtools.de/topic/2467/gibt-es-erfahrungen-mit-einbindung-in-wordpress/37)
Version: 1.2.1
Author: Jonathan Straub
Author URI: https://immanuel-online.de
*/
/**
 * HowTo: 
 * 1) Go to Settings -> ChurchTools Importer
 * 2) Set ChurchTools-URL, Category IDs and Max-Calendar Entries 
 * 3) Add Code to Homepage for eventlist, whereever you want: [churchtools_cal] or use attributes [churchtools_cal categories="42,43" events_per_page="12"]
 *    Add Code to Homepage for fullcallendar, whereever you want: [churchtools_fullcal] or use attributes [churchtools_fullcal categories="42,43"]
 * 4) Customize output in churchcal.php in lines 154-196 (list) and 198-291 (fullcalendar) if you like
 */


/*
  Adds Shortcode [churchtools_cal]  to print calendar
*/
function ctsync_cal_shortcode( $atts ) {
    do_action('ctsync_includeChurchcalSync');
    $atts = ctsync_sanitize_shortcode($atts);
    //$display_past  = filter_var( $atts['display_past'], FILTER_VALIDATE_BOOLEAN );
    return ctsync_printCalendarEvents(ctsync_getCalendarEvents(),$atts);
}
add_shortcode('churchtools_cal','ctsync_cal_shortcode');

//Adds shortcode [churchtools_event] to print single event; content is used for the link option
function ctsync_evt_shortcode($atts, $content = '') {
    do_action('ctsync_includeChurchcalSync');
    //let shortcodes inside this shortcode be parsed as well
    $content = do_shortcode($content);
    //fill in default values
    $atts = ctsync_sanitize_shortcode($atts);
    return ctsync_printSingleEvent(ctsync_getCalendarEvents(), $atts, $content);
}
add_shortcode('churchtools_evt', 'ctsync_evt_shortcode');
add_shortcode('churchtools_sub_evt', 'ctsync_evt_shortcode');

/*//Adds shortcode [churchtools_modal] to create modal
function ctsync_modal_shortcode($atts) {
    do_action('ctsync_includeChurchcalSync');
    //fill in default values
    $atts = ctsync_sanitize_shortcode($atts);
    return ctsync_printEventModal(ctsync_getCalendarEvents(), $atts);
}
add_shortcode('churchtools_modal', 'ctsync_modal_shortcode');
*/
add_action ('admin_menu', 'ctsync_setup_menu'  );
add_action('save_ctsync_settings', 'save_ctsync_settings' );
function ctsync_setup_menu() {
	add_options_page('ChurchTools Cal Importer','ChurchTools Importer','manage_options','churchtools-calsync','ctsync_dashboard');
	add_action('admin_init', 'register_ctsync_settings' );
}
function register_ctsync_settings(){
	register_setting( 'ctsync-group', 'ctsync_url');
	register_setting( 'ctsync-group', 'ctsync_userid');
	register_setting( 'ctsync-group', 'ctsync_logintoken');
	register_setting( 'ctsync-group', 'ctsync_ids');
	register_setting( 'ctsync-group', 'ctsync_maximport');
	if(!empty($_POST['ctsync_url'])){
		save_ctsync_settings();
	}
}
function ctsync_dashboard() {
	$saved_data =  get_option('ctsync_options');
	$lastupdated = get_transient('churchtools_calendar_lastupdated');
//	$saved_data = $saved_data ? unserialize($saved_data) : null ;
	include_once (plugin_dir_path( __FILE__ ) .  'dashboard/dashboard_view.php');
}

// this function will handle the ajax call
function save_ctsync_settings() {
	$data = [];
	$saved_data =  get_option('ctsync_options');
	$data['url'] = rtrim(trim($_POST['ctsync_url']),'/').'/';
	$data['userid'] = trim($_POST['ctsync_userid']);
	$data['logintoken'] = trim($_POST['ctsync_logintoken']);
	$ids=trim($_POST['ctsync_ids']);
	$data['ids']=[];
	foreach(preg_split('/\D/',$ids) as $id){
		if(intval($id)>0){
			$data['ids'][] = intval($id);
		}
	}
	sort($data['ids']);
	$data['maximport'] = trim($_POST['ctsync_maximport']);

	if($saved_data) {
		update_option( 'ctsync_options',  $data );
	}else{
		add_option( 'ctsync_options',  serialize($data) );
	}
	do_action('ctsync_includeChurchcalSync');
	ctsync_getUpdatedCalendarEvents();
}

wp_register_style ( 'ctstyle', plugins_url ( '/style.css', __FILE__ ) );
 /**
 * Schedule the event when the plugin is activated, if not already scheduled
 * and run it immediately for the first time
 */
register_activation_hook( __FILE__, 'ctsync_activation' );
function ctsync_activation() {
	if ( ! wp_next_scheduled ( 'ctsync_hourly_event' ) ) {
		wp_schedule_event( current_time( 'timestamp' ), 'hourly', 'ctsync_hourly_event' );
	}
}
/**
 * Hook the function to run every hour
 */
add_action('ctsync_hourly_event', 'do_this_ctsync_hourly');
function do_this_ctsync_hourly() {
    do_action('ctsync_includeChurchcalSync');
	if( function_exists('ctsync_getUpdatedCalendarEvents'))
		$result = ctsync_getUpdatedCalendarEvents();
}
add_action('ctsync_includeChurchcalSync', 'ctsync_includeChurchcalSync');
function ctsync_includeChurchcalSync(){
	include( plugin_dir_path( __FILE__ ) . 'churchcal.php');
	include( plugin_dir_path( __FILE__ ) . 'helper.php');
}

/**
 * Clear the scheduled event when the plugin is disabled
 */
register_deactivation_hook( __FILE__, 'ctsync_deactivation' );
function ctsync_deactivation() {
	wp_clear_scheduled_hook( 'ctsync_daily_event' );
	wp_clear_scheduled_hook( 'ctsync_hourly_event' );
}
?>
